package pl.kkosobudzki.common.android.fragment;

import android.app.Fragment;
import android.os.Bundle;

import java.lang.Class;

/**
 * Interface for managing fragments in activity.
 *
 * @author Krzysztof Kosobudzki
 */
public interface FragmentSwapper {

    /**
     * Init or restore fragment.
     * <p/>
     * If {@param savedInstanceState} is null fragment is initialized, restored otherwise.
     *
     * @param savedInstanceState state from activity
     * @param defaultFragment    default fragment class to initialize
     */
    void initOrRestore(Bundle savedInstanceState, Class<? extends Fragment> defaultFragment);

    /**
     * Init fragment.
     * <p/>
     * Fragment class is initialized with default, no-arg constructor.
     *
     * @param defaultFragment default fragment class to initialize
     * @throws {@link IllegalArgumentException} if could not initialize fragment
     * @throws {@link IllegalArgumentException} if {@link FragmentManager} is null
     */
    void init(Class<? extends Fragment> defaultFragment);

    /**
     * Restore fragment from bundle.
     *
     * @param savedInstanceState state from activity
     */
    void restore(Bundle savedInstanceState);

    /**
     * Swap to fragment class.
     * <p/>
     * If current fragment already exists in fragment manager nothing should happens.
     *
     * @param fragment fragment class to display
     * @throws {@link IllegalArgumentException} if could not initialize fragment
     * @throws {@link IllegalArgumentException} if {@link FragmentManager} is null
     */
    void swapTo(Class<? extends Fragment> fragment);
}
