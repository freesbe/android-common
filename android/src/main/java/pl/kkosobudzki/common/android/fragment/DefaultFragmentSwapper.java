package pl.kkosobudzki.common.android.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

import java.lang.Class;
import java.lang.Exception;
import java.lang.IllegalArgumentException;
import java.lang.Override;
import java.lang.String;
import java.lang.ref.WeakReference;

/**
 * Helper class for managing fragments in activity.
 *
 * @author Krzysztof Kosobudzki
 */
public class DefaultFragmentSwapper implements FragmentSwapper {
    private static final String CONTENT_FRAGMENT = "CONTENT_FRAGMENT";

    private final WeakReference<FragmentManager> mFragmentManagerReference;

    private final int mContainerId;

    /**
     * Constructor.
     *
     * @param fragmentManager fragment manager from activity
     * @param containerId id of frame layout where fragments are hold
     */
    public DefaultFragmentSwapper(final FragmentManager fragmentManager, final int containerId) {
        mFragmentManagerReference = new WeakReference<FragmentManager>(fragmentManager);

        mContainerId = containerId;
    }

    @Override
    public void initOrRestore(final Bundle savedInstanceState, final Class<? extends Fragment> defaultFragment) {
        if (savedInstanceState == null) {
            init(defaultFragment);
        } else {
            restore(savedInstanceState);
        }
    }

    @Override
    public void init(final Class<? extends Fragment> defaultFragment) {
        Fragment fragment = null;

        try {
            fragment = defaultFragment.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Could not initialize default fragment");
        }

        FragmentManager fragmentManager = mFragmentManagerReference.get();

        if (fragmentManager == null) {
            throw new IllegalArgumentException("FragmentManager is null");
        }

        fragmentManager
                .beginTransaction()
                .replace(mContainerId, fragment, CONTENT_FRAGMENT)
                .commit();
    }

    @Override
    public void restore(Bundle savedInstanceState) {
        // TODO
    }

    @Override
    public void swapTo(final Class<? extends Fragment> fragment) {
        FragmentManager fragmentManager = mFragmentManagerReference.get();

        if (fragmentManager == null) {
            throw new IllegalArgumentException("FragmentManager is null");
        }

        final Fragment current = fragmentManager.findFragmentByTag(CONTENT_FRAGMENT);

        if (current != null && current.getClass().getName().equals(fragment)) {
            // the same class already present in fragment manager
            return;
        }

        try {
            final Fragment f = fragment.newInstance();

            fragmentManager
                    .beginTransaction()
                    .replace(mContainerId, f, CONTENT_FRAGMENT)
                    .commit();
        } catch (Exception e) {
            throw new IllegalArgumentException("Could not initialize default fragment");
        }
    }
}
