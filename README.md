#Android-Common
Common library for Android based projects.


## Building

### With Gradle

The easiest way to build is to use `gradlew`:

1. Open terminal
2. Navigate to project directory
3. Type `./gradlew clean build`

To install library in local maven repository type:

`./gradlew clean install`

##License

    Copyright 2015 Krzysztof Kosobudzki

    Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.